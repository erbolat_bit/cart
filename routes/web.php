<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group([
  // 'middleware' => 'clientAuth'
], function () use ($router) {
  $router->get('order', ['as' => 'order.index', 'uses' => 'OrderController@index']);
  $router->post('order/store', ['as' => 'order.store', 'uses' => 'OrderController@store']);
  $router->post('order/update/{order}', ['as' => 'order.update', 'uses' => 'OrderController@update']);
  $router->get('order/{order:[0-9]+}', ['as' => 'order.show', 'uses' => 'OrderController@show']);
  $router->delete('order/{order:[0-9]+}', ['as' => 'order.destroy', 'uses' => 'OrderController@destroy']);
  $router->get('order/check/{order:[0-9]+}', ['as' => 'order.check.status', 'uses' => 'OrderController@checkStatus']);
  

  $router->get('order-item/{item}', ['as' => 'item.one', 'uses' => 'OrderController@getItem']);
  $router->post('order-item/confirm/{item}', ['as' => 'item.confirm', 'uses' => 'OrderController@confirmItem']);
  $router->post('order-item/review/{item}', ['as' => 'item.review', 'uses' => 'OrderController@reviewItem']);
  $router->delete('order-item/{item}', ['as' => 'item.destroy', 'uses' => 'OrderController@destroyItem']);

  $router->get('basket', ['as' => 'basket.index', 'uses' => 'BasketController@index']);
  $router->post('basket/store', ['as' => 'basket.store', 'uses' => 'BasketController@store']);
  $router->delete('basket/{basket}', ['as' => 'basket.destroy', 'uses' => 'BasketController@destroy']);

  $router->get('order/store-buyers/{store_id}', ['as' => 'order.buyers', 'uses' => 'OrderController@getStoreBuyers']);
  
  $router->get('transactions', ['as' => 'order.buyers', 'uses' => 'OrderController@transactions']);
  
  $router->post('balance/store', ['as' => 'balance.replenishment', 'uses' => 'BalanceController@store']);

  //Admin Routes -------->
  $router->group(['prefix' => 'admin', 'namespace' => 'Admin'], function () use ($router) {

    $router->group(['prefix' => 'order'], function () use ($router) {
      $router->get('/list', 'OrderController@index');
      $router->get('/show/{order:[0-9]+}', 'OrderController@show');
      $router->get('/show', 'OrderController@filter');
      $router->get('/statistic', 'OrderController@getOrderStatistic');

    });

    $router->group(['prefix' => 'basket'], function () use ($router) {
      $router->get('/list', 'BasketController@index');
      $router->get('/show/{basket:[0-9]+}', 'BasketController@show');
      $router->get('/show', 'BasketController@filter');
    });
  });
});
