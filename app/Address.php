<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    
    public $timestamps = false;
 
    protected $fillable = [
        'order_id',
        'store_id',
        'name',
        'phone',
        'address',
        'pickup',
    ];

    protected $casts = [
        'pickup' => 'boolean'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
