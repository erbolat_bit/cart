<?php

namespace App\Payment;

class BalanceReplenishment extends Bank
{

  public function totalSum()
  {
    return $this->order->items()->get()->sum('amount');
  }

  public function data(): array
  {
    $data = parent::data();

    $data['url']['success'] = config('payment.url.balance_success');
    $data['url']['redirect'] = config('payment.url.redirect_balance');

    return $data;
  }
}