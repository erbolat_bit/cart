<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Order as ResourcesOrder;
use App\Order;
use App\OrderItem;
use App\Transformer\OrderListTransformer;
use App\Transformer\OrderTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Traits\SendSuccessResponseTrait;

class OrderController extends Controller
{
  use SendSuccessResponseTrait;
  /**
   * Viewing All Orders.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    return ResourcesOrder::collection(Order::orderBy('created_at', 'desc')->paginate(10))
      ->additional($this->responseInformation());
  }

  /**
   * Showing One Order by Id.
   *
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $order)
  {
    $or = Order::find($order);
    if (!$or)
      throw new ModelNotFoundException();

    $res =  fractal()
      ->item($or)
      ->transformWith(new OrderTransformer)
      ->toArray();
    $stats = [
      'success' => true,
      'error' => false,
      'status_code' => 200
    ];
    return response()->json(array_merge($res, $stats));
  }


  /**
   * Filtering data.
   *
   * @return \Illuminate\Http\Response
   */
  public function filter(Request $request)
  {
    $filterabels = $request->except([
      'per_page',
      'order_by',
      'asc',
    ]);

    $query = Order::orderBy($request->order_by ?? 'created_at', $request->asc ?? 'desc');

    foreach ($filterabels as $key => $val) {
      if (is_null($val)) continue;
      $query = $query->where($key, $val);
    }

    $paginator = $query->paginate($request->per_page ?? 15);

    $Orders = $paginator->getCollection();

    return fractal()
      ->collection($Orders)
      ->transformWith(new OrderTransformer)
      ->toArray();
  }

  public function getOrderStatistic(Request $request)
  {
    $orders = OrderItem::select();
    if ($request->has('shop_id')) {
      $orders->where('store_id', $request->shop_id);
    }

    if ($request->from_date) {
      $orders->whereHas('order', function($query) use($request){
        $query->withTrashed()->whereDate('created_at', '>=', $request->from_date);
      });
    }

    if ($request->to_date) {
      $orders->whereHas('order', function($query) use($request){
        $query->withTrashed()->whereDate('created_at', '<=', $request->to_date);
      });
    }

    return $orders->count();
  }
}
