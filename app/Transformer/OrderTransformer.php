<?php

namespace App\Transformer;

use App\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{

  private $payment_response;

  public function __construct($payment_response = false)
  {
    $this->payment_response = $payment_response;
  }
  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [
    // 'items'
  ];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [];

  /**
   * Turn this item object into a generic array
   *
   * @return array
   */
  public function transform(Order $order)
  {
    $data = [
      'id' => $order->id,
      'user_id' => $order->user_id,
      'address' => $order->addresses,
      'payment' => (int) $order->payment,
      'status_text' => $order->statusMessage(),
      'status' => (int) $order->status,
      'created_at' => $order->created_at->format('d-m-Y h:i'),
      'items' => $order->items()->get()->groupBy('store_id'),
    ];

    if ($order->error_code) {
      $data['error_message'] = $order->payment_errors[array_search($order->payment, config('payment.methods'))][$order->error_code];
    }

    if ($this->payment_response) {
      $data['payment_response'] = $this->payment_response;
    }

    return $data;
  }

  public function includeItems(Order $order)
  {
    return $this->collection($order->items, new OrderItemTransformer);
  }
}
