<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{

    const TYPES = [ // типы скидки
        'coupon' => 1, // купон
        'stock' => 2 // акция
    ];

    public $timestamps = false;
    
    protected $fillable = [
        'discount_sum',
        'order_item_id',
        'type',
        'type_id'
    ];

    public function orderItem()
    {
        return $this->belongsTo('App\Order');
    }
}
