<?php

namespace App;

use App\Payment\PaymentInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Order extends Model
{

  use SoftDeletes;

  const STATUS_NEW = 0; // новый заказ
  const STATUS_PAID = 1; // заказ оплачен (оплата прошла успешно)
  const STATUS_CANCEL = 2; // заказ отменен
  const STATUS_ERROR = 3; // произошла ошибка
  const STATUS_WAITING = 4; // ожидание оплаты

  protected $fillable = [
    'user_id',
    'payment',
    'status',
    'error_code', // payment errors code ,
    'transaction_id'
  ];

  public $payment_errors_key = [
    1 => 'bank', // оплата через банк Buyk Ipak Yuli
    2 => 'balance_replenishment', // пополнение баланса
    3 => 'payment_from_blance', // оплата через личный счет баланса пользователья
    4 => 'payment_cash' // оплата налом
  ];

  public $status_messages = [
    self::STATUS_NEW => 'новый заказ',
    self::STATUS_PAID => 'заказ оплачен (оплата прошла успешно)',
    self::STATUS_CANCEL => 'заказ отменен',
    self::STATUS_ERROR => 'произошла ошибка',
    self::STATUS_WAITING => 'ожидание оплаты (оплата наличными)'
  ];

  public $payment_errors = [
    'bank' => [
      1 => 'Empty or incorrect header Content-Type',
      2 => 'Incorrect header Content-Type',
      3 => 'Empty header Key',
      4 => 'Incorrect header Key',
      5 => 'Empty or incorrect header Content-Length',
      6 => 'Empty request body',
      7 => 'Incorrect request parameters',
      8 => 'Inactive EPOS',
      9 => 'Empty content',
      10 => 'Client close window',
      11 => 'Транзакция (списание) не прошла',
      13 => 'Транзакция игнорирована по причине не корректного ввода три раза кода подтверждения',
      14 => 'ID транзакции повторяется',
      15 => 'Не авторизованный запрос',
      16 => 'Сумма операции меньше минимальной суммы'
    ],
    'balance_replenishment' => [
      1 => 'Empty or incorrect header Content-Type',
      2 => 'Incorrect header Content-Type',
      3 => 'Empty header Key',
      4 => 'Incorrect header Key',
      5 => 'Empty or incorrect header Content-Length',
      6 => 'Empty request body',
      7 => 'Incorrect request parameters',
      8 => 'Inactive EPOS',
      9 => 'Empty content',
      10 => 'Client close window',
      11 => 'Транзакция (списание) не прошла',
      13 => 'Транзакция игнорирована по причине не корректного ввода три раза кода подтверждения',
      14 => 'ID транзакции повторяется',
      15 => 'Не авторизованный запрос',
      16 => 'Сумма операции меньше минимальной суммы'
    ],
    'payment_from_blance' => [
      
    ],
    'payment_cash' => [

    ]
  ];

  public function setBankMethodErrorKeys(): void
  {
    foreach (config('payment.methods') as $key => $value) {
      $this->payment_errors[$value] = $key;
    }
  }

  public function setPaymentErrors(): void
  {
    foreach (config('payment.methods') as $key => $value) {
      $this->payment_errors[$key] = config('payment.errors')[$key];
    }
  }

  public function paymentObj(): PaymentInterface
  {
    $class = 'App\Payment\\' . str_replace('_', '', ucwords($this->getPaymentMethod(), '_'));
    return new $class;
  }

  public function statusMessage()
  {
    return $this->status_messages[(int) $this->status];
  }

  public function getErrorData()
  {
    return [
      'status' => $this->status,
      'message' => $this->status_messages[$this->status],
      'error' => $this->status != self::STATUS_NEW ? $this->payment_errors[$this->payment_errors_key[$this->payment]][$this->error_code] : 0
    ];
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function items()
  {
    return $this->hasMany('App\OrderItem');
  }

  public function addresses()
  {
    return $this->hasMany('App\Address');
  }

  public function scopeCompleted($query)
  {
    return $this->where('status', self::STATUS_PAID)->whereDoesntHave('items', function (Builder $query) {
      $query->where('status', OrderItem::STATUS_UNDELIVERED);
    });
  }

  public function scopeUndelivered($query)
  {
    return $this->where('status', self::STATUS_PAID)->whereHas('items', function ($query) {
      $query->where('status', OrderItem::STATUS_UNDELIVERED);
    });
  }

  public function scopeNew($query)
  {
    return $this->where('status', self::STATUS_NEW);
  }

  public function getPaymentMethod()
  {
    $methods = config('payment.methods');
    return array_search((int)$this->payment, $methods, true);
  }

}
