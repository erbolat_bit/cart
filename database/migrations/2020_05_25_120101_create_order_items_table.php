<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('order_items', function (Blueprint $table) {
      $table->id();
      $table->unsignedBigInteger('order_id');
      $table->integer('product_id')->nullable();
      $table->string('product_title')->nullable();
      $table->integer('store_id')->nullable();
      $table->string('store_title')->nullable();
      $table->decimal('amount', 19, 2);
      $table->string('currency');
      $table->decimal('discount')->nullable();
      $table->integer('quantity')->default(1);
      $table->integer('delivery_method')->nullable();
      $table->decimal('delivery_price')->default(0);

      $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('order_items');
  }
}
