<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Payment\BalanceReplenishment;
use App\Payment\Payment;
use App\Rules\Number;
use App\Transformer\OrderTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BalanceController extends Controller
{

  public function store(Request $request){
    
    $validator = $this->validated($request);

    if ($validator->fails()) {
      return response()->json([
        'message' => $validator->errors(),
        'success' => false,
        'error' => true,
        'status_code' => 422
      ]); 
    }

    $result = DB::transaction(function() use($request){
      try {
        $input = $request->input();
        $input['user_id'] = $request->user_id;
        $order = new Order($input);
        if (!config('payment.send')) {
          $input['status'] = Order::STATUS_PAID;
        }
        $order->save($input);
        
        $item = new OrderItem([
          'amount' => $input['amount'],
          'currency' => $input['currency'],
          'order_id' => $order->id,
        ]);
        $item->save();
    
        if (config('payment.send')) {
          $payment = new Payment(new BalanceReplenishment(), $request->header('Lang'));
          $response = $payment->send($order);
        } else {
          $response = false;
        }

        return [
          'order' => $order,
          'response' => $response
        ];
      } catch (\Exception $e) {
        throw $e;
      }
      
    });

   
    return fractal()
      ->item($result['order'])
      ->transformWith(new OrderTransformer($result['response']))
      ->toArray();
    
  }


  private function validated(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'payment' => [
        'integer',
        'required'
      ],
      'amount' => [
        new Number,
        'required'
      ],
      'currency' => [
        'string',
        'required'
      ]
    ]);

    return $validator;
  }
}