<?php

namespace App\Http\Controllers\Admin;

use App\Basket;
use App\Http\Controllers\Controller;
use App\Transformer\BasketTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use App\Traits\SendSuccessResponseTrait;

class BasketController extends Controller
{
  use SendSuccessResponseTrait;
  /**
   * Viewing All Baskets.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $paginator = Basket::orderBy('created_at', 'desc')->paginate(10);
    return $this->sendSuccessResponse($paginator);
  }

  /**
   * Showing One Basket by Id.
   *
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request, $basket)
  {
    $ba = Basket::find($basket);
    if (!$ba)
      throw new ModelNotFoundException();

    $res = fractal()
      ->item($ba)
      ->transformWith(new BasketTransformer)
      ->toArray();
    return $this->mergeStatusInformation($res);
  }


  /**
   * Filtering data.
   *
   * @return \Illuminate\Http\Response
   */
  public function filter(Request $request)
  {
    $filterabels = $request->except([
      'per_page',
      'order_by',
      'asc',
    ]);

    $query = Basket::orderBy($request->order_by ?? 'created_at', $request->asc ?? 'desc');
    foreach ($filterabels as $key => $val) {
      if (is_null($val)) continue;
      $query = $query->where($key, $val);
    }

    $paginator = $query->paginate($request->per_page ?? 15);
    $Basket = $paginator->getCollection();

    return fractal()
      ->collection($Basket, new BasketTransformer)
      ->paginateWith(new IlluminatePaginatorAdapter($paginator))
      ->serializeWith(new DataArraySerializer);
  }
}
