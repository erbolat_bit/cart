<?php

namespace App\Http\Controllers;

use App\Basket;
use App\Transformer\BasketTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Traits\SendSuccessResponseTrait;

class BasketController extends Controller
{
  use SendSuccessResponseTrait;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $baskets = Basket::where('user_id', $request->user_id)->get()->toArray();
    return response()->json(['data' => $baskets]);
  }


  /**
   *
   * @queryParams product_id required|integer
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'product_id' => 'integer|required'
    ]);

    $input = $request->input();
    $basket = Basket::where('product_id', $input['product_id'])->where('user_id', $input['user_id'])->first();
    if ($basket) {
      $basket->increment('quantity');
    } else {
      $basket = new Basket($input);
      $basket->save($input);
    }

    return fractal()
      ->item($basket)
      ->transformWith(new BasketTransformer)
      ->toArray();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Basket  $basket
   * @return \Illuminate\Http\Response
   */
  public function destroy($basket)
  {
    $basket = Basket::find($basket);
    if (!$basket)
      throw new ModelNotFoundException();

    $basket->delete();
    return $this->sendSuccessResponse();
  }
}
