<?php

namespace App\Payment;

class Payment
{

  private $payment;
  private $lang;

  public function __construct(PaymentInterface $payment, $lang)
  {
    $this->payment = $payment;
    $this->lang = $lang;
  }

  public function send($order)
  {
    $this->payment->setLang($this->lang);
    return $this->payment->send($order);
  }
}
