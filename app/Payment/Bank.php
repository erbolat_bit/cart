<?php

namespace App\Payment;

use App\Order;
use Illuminate\Support\Facades\DB;

class Bank extends BasePayment
{

  /**
   * Send payment
   * 
   * @params $order \App\Models\Order
   * 
   */
  public function send(Order $order)
  {
    $this->setOrder($order);
    $re = '/target="_blank"/';
    $html = html_entity_decode($this->client->post(config('payment.bank.api_url'), $this->data()));
    $response = preg_replace($re, 'target="_self"', $html);
    return $response;
  }


}
