<?php

namespace App\Http\Controllers;

use App\Address;
use App\Basket;
use App\Discount;
use App\Order;
use App\OrderItem;
use App\Payment\Bank;
use App\Payment\Payment;
use App\Rules\Number;
use App\Traits\SendSuccessResponseTrait;
use App\Transformer\OrderItemTransformer;
use App\Transformer\OrderListTransformer;
use App\Transformer\OrderTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;

class OrderController extends Controller
{

  use SendSuccessResponseTrait;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $orders = Order::orderBy('created_at', 'desc');
    $complated = $orders->where('user_id', '=', $request->user_id);
    $delivery = $orders->undelivered()->where('user_id', '=', $request->user_id);

    if ($request->delivery) {
      $paginator = $delivery->paginate();
    } else {
      $paginator = $complated->paginate();
    }

    $count = [
      'complated_count' => $complated->count(),
      'delivery_count' => $delivery->count()
    ];

    $collection = collect([
      'orders' => $paginator->getCollection(),
      'pagination' => $paginator
    ]);
    $result = $collection->merge(['count' => $count]);

    return fractal()
      ->item($result)
      ->transformWith(new OrderListTransformer)
      ->toArray();
  }

  public function transactions(Request $request)
  {
    $user_id = $request->user_id;
    $transactions = OrderItem::with(['order' => function($order){
      $order->withTrashed();
    }])->whereHas('order', function ($query) use ($user_id) {
      $query->select('created_at')->where('user_id', $user_id)->where('status', Order::STATUS_PAID)->withTrashed();
    })->paginate(5);

    return fractal()
      ->collection($transactions->getCollection(), new OrderItemTransformer, 'data')
      ->paginateWith(new IlluminatePaginatorAdapter($transactions))
      ->serializeWith(new DataArraySerializer);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $this->validated($request);

    if ($validator->fails()) {
      return response()->json([
        'message' => $validator->errors(),
        'success' => false,
        'error' => true,
        'status_code' => 422
      ]);
    }

    $result = DB::transaction(function () use ($request) {
      try {
        $input = $request->input();
        $input['user_id'] = $request->user_id;
        $order = new Order($input);
        if (!config('payment.send')) {
          $input['status'] = Order::STATUS_PAID;
        }

        $order->save($input);

        $addresses = [];

        foreach ($request->address as $key => $address) {
          $addresses[] = new Address($address);
        }

        $order->addresses()->saveMany($addresses);

        foreach ($request->products as $key => $value) {
          $value['order_id'] = $order->id;
          $item = new OrderItem($value);
          if ($item->save() && isset($value['discount']) && !empty($value['discount'])) {
            $discounts = [];
            foreach ($value['discount'] as $key => $value) {
              $discounts[] = new Discount($value);
            }
            $item->discounts()->saveMany($discounts);
          }
        }

        if (config('payment.send')) {
          $payment = new Payment($order->paymentObj(), $request->header('Lang'));
          $response = $payment->send($order);
        } else {
          $response = false;
        }

        Basket::where('user_id', $order->user_id)->delete();

        return [
          'order' => $order,
          'response' => $response
        ];
      } catch (\Exception $e) {
        throw $e;
      }
    });

    return fractal()
      ->item($result['order'])
      ->transformWith(new OrderTransformer($result['response']))
      ->toArray();
  }

  public function update(Request $request, $order)
  {
    $validator = Validator::make($request->all(), [
      'status' => 'required|integer'
    ]);

    if ($validator->fails()) {
      return response()->json(['error' => false, 'success' => true, 'message' => $validator->errors()]);
    }

    $order = Order::with(['items'])->where('id', $order)->first();
    $update = $order->update([
      'status' => $request->status,
      'error_code' => $request->error_code,
      'transaction_id' => $request->transaction_id
    ]);


    if ($update) {
      if ($request->has('balance') && $request->balance) {

        $result['order'] = $order->toArray();
        $result['order']['total_sum'] = $order->items->sum('amount');

        return response()->json(['error' => false, 'success' => true, 'data' => $result]);
      } else {
        $res = DB::select(
          'SELECT store_id, 
                          SUM(amount + delivery_price - dis_sum) total_sum 
                          FROM (
                            SELECT store_id, amount, delivery_price,(SELECT COALESCE(SUM(discount_sum), 0) 
                              FROM discounts 
                              WHERE order_items.id = discounts.order_item_id
                          ) 
                              AS dis_sum 
                              FROM order_items 
                              WHERE order_id = ?) 
                              AS t1 
                              GROUP BY store_id',
          [$order->id]
        );
        $data = json_decode(json_encode($res), true);

        Basket::where('user_id', $order->user_id)->delete();

        $result['data'] = $data;

        $or = $order->toArray();
        $or['items'] = $order->items->groupBy('store_id');

        $result['order'] = $or;
        return response()->json(['error' => false, 'success' => true, 'data' => $result]);
      }
    } else {
      return response()->json(['error' => true, 'success' => false, 'data' => []]);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Order  $order
   * @return \Illuminate\Http\Response
   */
  public function show($order)
  {
    $order = Order::find($order);
    if (!$order)
      throw new ModelNotFoundException();

    return fractal()
      ->item($order)
      ->transformWith(new OrderTransformer)
      ->toArray();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Order  $order
   * @return \Illuminate\Http\Response
   */
  public function destroy($order)
  {
    $order = Order::find($order);
    if (!$order)
      throw new ModelNotFoundException();

    $order->delete();

    return $this->sendSuccessResponse();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Order  $order
   * @return \Illuminate\Http\Response
   */
  public function getItem($item)
  {
    $item = OrderItem::find($item);
    if (!$item)
      throw new ModelNotFoundException();

    return fractal()
      ->item($item)
      ->transformWith(new OrderItemTransformer)
      ->toArray();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Order  $order
   * @return \Illuminate\Http\Response
   */
  public function destroyItem($item)
  {
    $order_item = OrderItem::find($item)->delete();
    if (!$order_item)
      throw new ModelNotFoundException();

    return $this->sendSuccessResponse();
  }

  public function getStoreBuyers(Request $request, $store_id)
  {
    $user_ids = Order::withTrashed()->whereHas('items', function ($query) use ($store_id) {
      $query->where('store_id', $store_id);
    })->get()->pluck('user_id')->unique();

    return response()->json(['data' => $user_ids]);
  }

  public function confirmItem($item)
  {
    $item = OrderItem::find($item);
    if (!$item)
      throw new ModelNotFoundException();

    $item->update(['status' => OrderItem::STATUS_DELIVERED]);
    return fractal()
      ->item($item)
      ->transformWith(new OrderItemTransformer)
      ->toArray();
  }

  public function reviewItem($item)
  {
    $item = OrderItem::find($item);
    if (!$item)
      throw new ModelNotFoundException();

    $item->update(['has_review' => true]);
    return fractal()
      ->item($item)
      ->transformWith(new OrderItemTransformer)
      ->toArray();
  }

  public function checkStatus(Request $request, $order)
  {

    $order = Order::find($order);
    $data = $order->getErrorData();
    return response()->json($data);
  }

  public function validated(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'address' => [
        'array',
        'required'
      ],
      'address.*.address' => [
        'string',
        'nullable'
      ],
      'address.*.phone' => [
        'string',
        'nullable'
      ],
      'address.*.name' => [
        'string',
        'nullable'
      ],
      'address.*.pickup' => [
        'boolean',
        'required'
      ],
      'address.*.store_id' => [
        'integer',
        'required'
      ],
      'payment' => [
        'integer',
        'required'
      ],
      'products' => [
        'required',
        'array'
      ],
      'products.*.product_id' => [
        'required',
        'integer'
      ],
      'products.*.amount' => [
        new Number,
        'required'
      ],
      'products.*.currency' => [
        'string',
        'required'
      ],
      'products.*.discount' => [
        'nullable',
        'array'
      ],
      'products.*.discount.*.discount_sum' => [
        new Number,
        'required'
      ],
      'products.*.discount.*.type' => [
        'nullable',
        'integer',
        'in:' . implode(',', array_values(Discount::TYPES))
      ],
      'products.*.discount.*.type_id' => [
        'nullable',
        'integer'
      ],
      'products.*.quantity' => [
        'integer',
        'required'
      ],
      'products.*.delivery_method' => [
        'integer',
        'required'
      ],
      'products.*.delivery_price' => [
        new Number,
        'nullable'
      ],
    ]);

    return $validator;
  }
}
