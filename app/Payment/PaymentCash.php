<?php

namespace App\Payment;

use App\Order;
use Exception;

class PaymentCash extends BasePayment
{

  public function send(Order $order)
  {
    $this->setOrder($order);

    $order->status = Order::STATUS_WAITING;
    if ($order->save()) {
      return true;
    }else{
      throw new Exception('Error cash payment');
    };
  }
}