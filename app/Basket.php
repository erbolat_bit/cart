<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    
    protected $connection = 'mysql';

    protected $fillable = [
        'user_id',
        'product_id',
        'quantity'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
