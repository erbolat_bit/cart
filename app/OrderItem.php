<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    const STATUS_UNDELIVERED = 0; // заказ в ожидании доставки
    const STATUS_DELIVERED = 1; // заказ получен

    protected $fillable = [
        'order_id',
        'product_id',
        'product_title',
        'store_id',
        'store_title',
        'amount',
        'currency',
        'quantity',
        'delivery_method',
        'delivery_price',
        'status',
        'has_review',
        'photo'
    ];

    protected $casts = [
        'has_review' => 'boolean'
    ];

    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function discounts()
    {
        return $this->hasMany('App\Discount');
    }
}
