<?php

namespace App\Transformer;

use App\Basket;
use League\Fractal\TransformerAbstract;

class BasketTransformer extends TransformerAbstract
{

  protected $defaultIncludes = [];
  protected $availableIncludes = [];

  public function transform(Basket $basket)
  {
    return [
      'id' => $basket->id,
      'user_id' => $basket->user_id,
      'product_id' => $basket->product_id,
      'quantity' => $basket->quantity
    ];
  }
}
