<?php

namespace App\Payment;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

abstract class BasePayment implements PaymentInterface
{
  public $client;
  public $lang;
  public $order;

  public function __construct()
  {
    $this->client = Http::withHeaders([
      'Content-type' => 'application/json; charset="utf-8"',
      'X-API-Key' => config('payment.bank.api_token')
    ]);
  }

  /**
   * setOrder
   *
   * @param  mixed $value
   * @return void
   */
  public function setOrder($value)
  {
    $this->order = $value;
  }


  /**
   * Set payment Lang
   * 
   * @params $request Illuminate\Http\Request;
   * 
   */
  public function setLang($value): void
  {
    $this->lang = $value ?? 'ru';
  }
  

  /**
   * Transaction data
   * 
   */
  public function data(): array
  {
    $order = $this->order;
    $data = [
      "transactionID" => $order->id,
      "amount" => ($this->totalSum() * 100),
      "terminal_num" => 1,
      "lang" => $this->lang,
      "user_id" => $order->user_id,
      "url" => [
        "success" => config('payment.url.success'),
        "fail" => config('payment.url.fail'),
        "redirect" => config('payment.url.redirect')
      ]
    ];
    return $data;
  }


  /**
   * Order total sum
   * 
   * return integer
   */
  public function totalSum()
  {
    $order = $this->order;
    $res = DB::select('SELECT 
    SUM(
        amount + delivery_price - dis_sum
    ) total_sum 
    FROM 
    (
        SELECT store_id, amount, delivery_price,
        (
            SELECT COALESCE(SUM(discount_sum), 0) 
            FROM discounts 
            WHERE order_items.id = discounts.order_item_id
        ) AS dis_sum
        FROM order_items 
        WHERE order_id = ?
    ) AS t1', [$order->id]);
    return json_decode(json_encode($res), true)[0]['total_sum'];
  }
}
