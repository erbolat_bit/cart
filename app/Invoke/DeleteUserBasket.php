<?php

namespace App\Invoke;

use Illuminate\Support\Facades\DB;

class DeleteUserBasket
{
  
  public function __invoke()
  {
    DB::table('baskets')->whereMonth('created_at', '>', (date('m', time()) - 1))->delete();
  }
}
