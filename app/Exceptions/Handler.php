<?php

namespace App\Exceptions;

use ErrorException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
  /**
   * A list of the exception types that should not be reported.
   *
   * @var array
   */
  protected $dontReport = [
    AuthorizationException::class,
    HttpException::class,
    ModelNotFoundException::class,
    ValidationException::class,
  ];

  /**
   * Report or log an exception.
   *
   * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
   *
   * @param  \Throwable  $exception
   * @return void
   *
   * @throws \Exception
   */
  public function report(Throwable $e)
  {
    parent::report($e);

    // $error = [
    // 	'message' => $e->getMessage(),
    // 	'content' => array_merge(
    // 			$this->exceptionContext($e),
    // 			$this->context(),
    // 			['exception' => $e]
    // 	)
    // ];

    // logger()->error(
    // 	$error['message'],
    // 	$error['content']
    // );
    // if (config('mail.send_errors')) {
    // 	Mail::to(config('mail.admin_email'))->queue(new ExceptionMail($error));
    // }
  }

  /**
   * Render an exception into an HTTP response.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Throwable  $exception
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Throwable
   */
  public function render($request, Throwable $exception)
  {
    if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
      return response()->json([
        'data' => [],
        'message' => "Not found",
        'success' => false,
        'error' => true,
        'status_code' => 405
      ]);
    }

    if ($exception instanceof NotFoundHttpException && $request->wantsJson()) {
      return response()->json([
        'data' => [],
        'message' => "Page Not found",
        'success' => false,
        'error' => true,
        'status_code' => 404
      ]);
    }

    if ($exception instanceof ValidationException && $request->wantsJson()) {
      return response()->json([
        'data' => [],
        'message' => $exception->errors(),
        'success' => false,
        'error' => true,
        'status_code' => $exception->status
      ]);
    }

    if ($exception instanceof QueryException && $request->wantsJson()) {
      return response()->json([
        'data' => [],
        'message' => 'Wrong Search Details are given',
        'success' => false,
        'error' => true,
        'status_code' => 422
      ]);
    }

    if ($exception instanceof ErrorException && $request->wantsJson()) {
      info($exception->getMessage());
      return response()->json([
        'data' => [],
        'message' => $exception->getMessage(),
        'success' => false,
        'error' => true,
        'status_code' => 500
      ]);
    }

    if ($exception instanceof HttpException && $request->wantsJson()) {
      if ($exception->getStatusCode() == 401) {
        return response()->json([
          'data' => [],
          'message' => 'Unauthorized client',
          'success' => false,
          'error' => true,
          'status_code' => 401
        ]);
      }
    }

    return parent::render($request, $exception);
  }
}
