<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderItemSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();

    $order_items = array();
    for ($i = 0; $i < 100; $i++) {
      $order_items[] = [
        'order_id' => rand(50, 100),
        'product_id' => rand(1, 100),
        'product_title' => $faker->text(30),
        'store_id' => rand(1, 100),
        'store_title' => $faker->text(20),
        'amount' => rand(1, 10000000),
        'currency' =>  rand(1, 3),
        'quantity' =>  rand(1, 9999),
        'delivery_price' => rand(1000, 200000)
      ];
    }
    DB::table('order_items')->insert($order_items);
  }
}
