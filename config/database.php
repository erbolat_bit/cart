<?php

return [

  'default' => 'mysql',
  'migrations' => 'migrations',
  'connections' => [
    'mysql' => [
      'driver' => 'mysql',
      'host' => env('DB_HOST', 'localhost'),
      'database' => env('DB_DATABASE'),
      'username' => env('DB_USERNAME'),
      'password' => env('DB_PASSWORD'),
      'port' => env('DB_PORT'),
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
    ],
  
    'mysql2' => [
      'driver' => 'mysql',
      'host' => env('DB2_HOST', 'localhost'),
      'database' => env('DB2_DATABASE'),
      'username' => env('DB2_USERNAME'),
      'password' => env('DB2_PASSWORD'),
      'port' => env('DB2_PORT'),
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
    ],
  ]

];