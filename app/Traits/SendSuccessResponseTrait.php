<?php

namespace App\Traits;

trait SendSuccessResponseTrait
{
  public function sendSuccessResponse($data = [], $stat_c = 200)
  {
    return response()->json([
      'data' => $data,
      'success' => true,
      'error' => false,
      'status_code' => $stat_c
    ], $stat_c);
  }

  public function responseInformation($status_c = 200): array
  {
    return [
      'success' => true,
      'error' => false,
      'status_code' => $status_c
    ];
  }

  public function mergeStatusInformation(array $data, int $status_code = 200): array
  {
    $stat_info = [
      'success' => true,
      'error' => false,
      'status_code' => $status_code
    ];
    return array_merge($data, $stat_info);
  }
}
