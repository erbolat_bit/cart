<?php

namespace App\Payment;

use App\Order;

class PaymentFromBlance extends BasePayment
{

  public function setLang($value): void
  {
    $this->lang = 'ru';
  }

  public function send(Order $order)
  {
    $this->setOrder($order);
    return [
      'from_balance' => 1,
      'total_sum' => $this->totalSum()
    ];
  }
}