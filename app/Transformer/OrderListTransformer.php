<?php

namespace App\Transformer;

use Illuminate\Support\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\TransformerAbstract;

class OrderListTransformer extends TransformerAbstract
{


  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [];

  /**
   * Turn this item object into a generic array
   *
   * @return array
   */
  public function transform(Collection $collection)
  {
    return [
      'order' => fractal()
        ->collection($collection['orders'], new OrderTransformer, 'data')
        ->paginateWith(new IlluminatePaginatorAdapter($collection['pagination']))
        ->serializeWith(new DataArraySerializer),
      'count' => $collection['count']
    ];
  }
}
