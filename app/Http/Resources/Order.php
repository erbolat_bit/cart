<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    $data = [
      'id' => $this->id,
      'user_id' => $this->user_id,
      'address_id' => $this->address_id,
      'payment' => $this->getPaymentMethod(),
      'status_text' => $this->statusMessage(),
      'status' => $this->status,
      'transaction_id' => $this->transaction_id,
      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
      'deleted_at' => $this->deleted_at,
    ];

    if ($this->error_code) {
      $data['error_message'] = $this->payment_errors['bank'][$this->error_code];
    }

    return $data;
  }
}
