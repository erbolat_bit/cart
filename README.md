# Uzexpress gateway api v1

## Installation

git clone git@bitbucket.org:erbolat_bit/cart.git

```bash
composer install
```

Copy .env.example to .env
```bash
cp .env.example .env
```
```bash
php artisan key:generate
```

Config DB
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
### Migration

```python
php artisan migrate --seed
```

### Cron

```
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```