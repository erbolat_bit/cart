<?php

return [
  'send' => env('PAYMENT_SEND', false),
  'methods' => [
    'bank' => 1, // оплата через банк Buyk Ipak Yuli
    'balance_replenishment' => 2, // пополнение баланса
    'payment_from_blance' => 3, // оплата через личный счет баланса пользователья
    'payment_cash' => 4 // оплата налом
  ],
  'bank' => [
    'api_url' => 'https://wi.ipakyulibank.uz/acquiring/hJaAGAA/Uz5QszX1kA9J6C6A7UtYScICvmVZ/',
    'api_token' => 'xURapP+H32+e1XoaRZroE9s8wboARBd8KFhPKnKDIYQPxrftheOVx3R51LHc1/3etGkUUvoPriDaF1ZMmy1jz8MfdJau6pCl13BF5WWL/ijR5cgcBcFW0F1AT=irBpK0'
  ],
  'errors' => [
    'bank' => [
      1 => 'Empty or incorrect header Content-Type',
      2 => 'Incorrect header Content-Type',
      3 => 'Empty header Key',
      4 => 'Incorrect header Key',
      5 => 'Empty or incorrect header Content-Length',
      6 => 'Empty request body',
      7 => 'Incorrect request parameters',
      8 => 'Inactive EPOS',
      9 => 'Empty content',
      10 => 'Client close window',
      11 => 'Транзакция (списание) не прошла',
      13 => 'Транзакция игнорирована по причине не корректного ввода три раза кода подтверждения',
      14 => 'ID транзакции повторяется',
      15 => 'Не авторизованный запрос',
      16 => 'Сумма операции меньше минимальной суммы'
    ],
    'balance_replenishment' => [
      1 => 'Empty or incorrect header Content-Type',
      2 => 'Incorrect header Content-Type',
      3 => 'Empty header Key',
      4 => 'Incorrect header Key',
      5 => 'Empty or incorrect header Content-Length',
      6 => 'Empty request body',
      7 => 'Incorrect request parameters',
      8 => 'Inactive EPOS',
      9 => 'Empty content',
      10 => 'Client close window',
      11 => 'Транзакция (списание) не прошла',
      13 => 'Транзакция игнорирована по причине не корректного ввода три раза кода подтверждения',
      14 => 'ID транзакции повторяется',
      15 => 'Не авторизованный запрос',
      16 => 'Сумма операции меньше минимальной суммы'
    ],
    'payment_from_blance' => [
      
    ],
    'payment_cash' => []
  ],
  'url' => [
    'balance_success' => 'https://gateway-v1.uzexpress.ru/v1/api/payment/balance/success',
    'success' => 'https://gateway-v1.uzexpress.ru/v1/api/payment/success',
    'fail' => 'https://gateway-v1.uzexpress.ru/v1/api/payment/fail',
    'redirect' => 'https://uzexpress.ru/ru/my-buynow/my-basket',
    'redirect_balance' => 'https://uzexpress.ru/ru/my-buynow',
  ],
  'sum_query' => '(amount + delivery_price)'
];
