<?php

namespace App\Payment;

use App\Order;

interface PaymentInterface{

  /**
   * Send payment
   * 
   * @params $order \App\Models\Order
   * 
   */
  public function send(Order $order);

  /**
   * Set payment lang
   *
   * @param  string (ru|en|uz) $value
   * @return void
   */
  public function setLang(string $value): void;

}