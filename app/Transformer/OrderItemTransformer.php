<?php 

namespace App\Transformer;

use App\Order;
use App\OrderItem;
use League\Fractal\TransformerAbstract;

class OrderItemTransformer extends TransformerAbstract
{
  
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(OrderItem $item)
    {
        return [
          'id' => $item->id,
          'order_id' => $item->order_id,
          'product_id' => $item->product_id,
          'product_title' => $item->product_title ?? 'пополнение баланса',
          'store_id' => $item->store_id,
          'store_title' => $item->store_title,
          'amount' => $item->amount,
          'currency' => $item->currency,
          'discount' => $item->discount,
          'quantity' => $item->quantity,
          'delivery_method' => $item->delivery_method,
          'delivery_price' => $item->delivery_price,
          'status' => $item->status,
          'has_review' => $item->has_review,
          'photo' => $item->photo,
          'created_at' => $item->order->created_at->format('Y-m-d'),
          'transaction_status' => $item->order->status
        ];
    }

}